<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CustomerPackage;
use App\PackagePaymentDetail;
use Auth;
use Session;
Use App\User;

class CustomerPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer_packages = CustomerPackage::all();
        return view('packages.index',compact('customer_packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('packages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer_package = new CustomerPackage;
        $customer_package->name = $request->name;
        $customer_package->amount = $request->amount;
        $customer_package->product_upload = $request->product_upload;
        if($request->hasFile('logo')){
            $customer_package->logo = $request->file('logo')->store('uploads/customer_package');
        }

        if($customer_package->save()){
            flash(__('Package has been inserted successfully'))->success();
            return redirect()->route('customer_packages.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer_package = CustomerPackage::findOrFail(decrypt($id));
        return view('packages.edit', compact('customer_package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer_package = CustomerPackage::findOrFail($id);
        $customer_package->name = $request->name;
        $customer_package->amount = $request->amount;
        $customer_package->product_upload = $request->product_upload;
        if($request->hasFile('logo')){
            $customer_package->logo = $request->file('logo')->store('uploads/customer_package');
        }

        if($customer_package->save()){
            flash(__('Package has been updated successfully'))->success();
            return redirect()->route('customer_packages.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer_package = CustomerPackage::findOrFail($id);
        if(CustomerPackage::destroy($id)){
            if($customer_package->logo != null){
                //unlink($customer_package->logo);
            }
            flash(__('Package has been deleted successfully'))->success();
            return redirect()->route('customer_packages.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    public function purchase_package(Request $request)
    {
        $data['customer_package_id'] = $request->customer_package_id;
        $data['payment_method'] = $request->payment_option;

        $request->session()->put('payment_type', 'package_payment');
        $request->session()->put('payment_data', $data);

        if($request->payment_option == 'paypal'){
            $paypal = new PaypalController;
            return $paypal->getCheckout();
        }
        elseif ($request->payment_option == 'stripe') {
            $stripe = new StripePaymentController;
            return $stripe->stripe();
        }
    }

    public function purchase_payment_done($payment_data, $payment){
        $user = User::findOrFail(Auth::user()->id);
        $user->customer_package_id = Session::get('payment_data')['customer_package_id'];
        $customer_package = CustomerPackage::findOrFail(Session::get('payment_data')['customer_package_id']);
        $user->remaining_uploads += $customer_package->product_upload;
        $user->save();

        $package_payment_details = new PackagePaymentDetail;
        $package_payment_details->user_id = Auth::user()->id;
        $package_payment_details->package_id = Session::get('payment_data')['customer_package_id'];
        $package_payment_details->payment_method = Session::get('payment_data')['payment_method'];
        $package_payment_details->payment_details = $payment;
        $package_payment_details->save();
        // dd($package_payment_details);
        flash(__('Payment completed'))->success();
        return redirect()->route('home');
    }

}
