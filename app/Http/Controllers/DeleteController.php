<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Subcategory;
use App\SubSubCategory;
use App\Brand;
use App\Product;
use App\CustomerProduct;

class DeleteController extends Controller
{
    public function index($type)
    {
        if ($type == 'delete_all_category') {
            return view('deletes.delete_all_contents', compact('type'));
        }
        elseif ($type == 'delete_all_subcategories') {
            return view('deletes.delete_all_contents', compact('type'));
        }
        elseif ($type == 'delete_all_subsubcategories') {
            return view('deletes.delete_all_contents', compact('type'));
        }
        elseif ($type == 'delete_all_brands') {
            return view('deletes.delete_all_contents', compact('type'));
        }
        elseif ($type == 'delete_all_classified') {
            return view('deletes.delete_all_contents', compact('type'));
        }
    }

    public function delete_all_content($type)
    {
        if ($type == 'delete_all_category') {
            $category = Category::truncate();
            $subcategory = Subcategory::truncate();
            $subsubcategory = SubSubCategory::truncate();
            $brand = Brand::truncate();
            $classified_product = CustomerProduct::truncate();
            $product = Product::truncate();
            flash(__('All Category and Category related all things have been deleted successfully'))->success();
            return redirect()->route('admin.dashboard');
        }
        elseif ($type == 'delete_all_subcategories') {
            $subcategory = Subcategory::truncate();
            $subsubcategory = SubSubCategory::truncate();
            $brand = Brand::truncate();
            $classified_product = CustomerProduct::truncate();
            $product = Product::truncate();
            flash(__('All Subcategory and Subcategory related all things have been deleted successfully'))->success();
            return redirect()->route('admin.dashboard');
        }
        elseif ($type == 'delete_all_subsubcategories') {
            $subsubcategory = SubSubCategory::truncate();
            $brand = Brand::truncate();
            $classified_product = CustomerProduct::truncate();
            $product = Product::truncate();
            flash(__('All SubSubCategory and SubSubCategory related all things have been deleted successfully'))->success();
            return redirect()->route('admin.dashboard');
        }
        elseif ($type == 'delete_all_brands') {
            $brand = Brand::truncate();
            $classified_product = CustomerProduct::truncate();
            $product = Product::truncate();
            flash(__('All Brands have been deleted successfully'))->success();
            return redirect()->route('admin.dashboard');
        }
        elseif ($type == 'delete_all_classified') {
            $classified_product = CustomerProduct::truncate();
            $product = Product::truncate();
            flash(__('All Clissified Products have been deleted successfully'))->success();
            return redirect()->route('admin.dashboard');
        }
    }
}
