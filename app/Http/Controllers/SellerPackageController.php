<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SellerPackage;
use App\SellerPackagePayment;
use Auth;
use Session;
Use App\User;
use App\Seller;

class SellerPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seller_packages = SellerPackage::all();
        return view('seller_packages.index', compact('seller_packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seller_package = SellerPackage::findOrFail(decrypt($id));
        return view('seller_packages.edit', compact('seller_package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $seller_package = SellerPackage::findOrFail($id);
        $seller_package->name = $request->name;
        $seller_package->amount = $request->amount;
        $seller_package->basic_product = $request->basic_product_upload;
        $seller_package->banner_uploads = $request->banner_upload;
        if($request->hasFile('logo')){
            $seller_package->logo = $request->file('logo')->store('uploads/seller_package');
        }
        if ($seller_package->save()) {
            flash(__('Package has been updated successfully'))->success();
            return redirect()->route('seller_packages.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function purchase_package(Request $request)
    {
        $data['seller_package_id'] = $request->seller_package_id;
        $data['payment_method'] = $request->payment_option;

        $request->session()->put('payment_type', 'seller_package_payment');
        $request->session()->put('payment_data', $data);

        if($request->payment_option == 'paypal'){
            $paypal = new PaypalController;
            return $paypal->getCheckout();
        }
        elseif ($request->payment_option == 'stripe') {
            $stripe = new StripePaymentController;
            return $stripe->stripe();
        }
    }

    public function purchase_payment_done($payment_data, $payment){
        $seller = Auth::user()->seller;
        $seller->seller_package_id = Session::get('payment_data')['seller_package_id'];
        $seller_package = SellerPackage::findOrFail(Session::get('payment_data')['seller_package_id']);
        $seller->remaining_uploads += $seller_package->basic_product;
        $seller->save();

        $package_payment_details = new SellerPackagePayment;
        $package_payment_details->user_id = Auth::user()->id;
        $package_payment_details->package_id = Session::get('payment_data')['seller_package_id'];
        $package_payment_details->payment_method = Session::get('payment_data')['payment_method'];
        $package_payment_details->payment_details = $payment;
        $package_payment_details->save();
        // dd($package_payment_details);
        flash(__('Payment completed'))->success();
        return redirect()->route('seller_packages_list_show');
    }
}
