@extends('layouts.app')

@section('content')

<div class="col-lg-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Ticket Details')}}</h3>
        </div>

        <div class="panel-body">
            <div class="form-group row">
                <label class="col-lg-2 control-label" for="subject"><strong>{{__('Subject')}}</strong></label>
                <div class="col-lg-9">
                    <p class="form-control">{{$ticket->subject}}</p>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-2 control-label" for="subject"><strong>{{__('Deatils')}}</strong></label>
                <div class="col-lg-9">
                <p class="form-control">{{$ticket->details}}</p>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-2 control-label" for="subject"><strong>{{__('Leave A Reply')}}</strong></label>
                <div class="col-lg-9">
                    <form class="form-horizontal" action="{{route('support_ticket.admin_store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                        <input type="hidden" name="ticket_id" value="{{$ticket->id}}">
                        <textarea class="editor" name="reply" required></textarea>
                        <div class="panel-footer text-right">
                            <button class="btn btn-purple" type="submit">{{__('Send Reply')}}</button>
                        </div>
                    </form>
                </div>
            </div>
            @foreach($ticket_replies as $ticketreply)
                <div class="form-group">
                    <a class="media-left" href="#"><img class="img-circle img-sm" alt="Profile Picture" src="{{ asset($ticketreply->user->avatar_original) }}">
                    </a>
                    <div class="media-body">
                        <div class="comment-header">
                            <a href="#" class="media-heading box-inline text-main text-bold">{{ $ticketreply->user->name }}</a>
                            <p class="text-muted text-sm">{{$ticketreply->created_at}}</p>
                        </div>
                        <p>
                            @php
                                echo $ticketreply->reply;
                            @endphp
                        </p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

@endsection
