@php
    $coupon_details = json_decode($coupon->details);
@endphp

<div class="panel-heading">
    <h3 class="panel-title">{{__('Update Your Product Base Coupon')}}</h3>
</div>
<div class="form-group">
    <label class="col-lg-3 control-label" for="coupon_code">{{__('Coupon code')}}</label>
    <div class="col-lg-9">
        <input type="text" placeholder="{{__('Coupon code')}}" id="coupon_code" name="coupon_code" class="form-control" value="{{ $coupon->code }}" required>
    </div>
</div>
<div class="form-group">
   <label class="col-lg-3 control-label">{{__('Category')}}</label>
   <div class="col-lg-9">
      <select class="form-control demo-select2" name="category_id" id="category_id" required>
         @foreach(\App\Category::all() as $key => $category)
             <option value="{{$category->id}}" @if ($coupon_details->cat_id == $category->id) selected  @endif>{{$category->name}}</option>
         @endforeach
      </select>
   </div>
</div>
<div class="form-group" id="subcategory">
   <label class="col-lg-3 control-label">{{__('Subcategory')}}</label>
   <div class="col-lg-9">
      <select class="form-control demo-select2" name="subcategory_id" id="subcategory_id" required>

      </select>
   </div>
</div>
<div class="form-group" id="subsubcategory">
   <label class="col-lg-3 control-label">{{__('Subsubcategory')}}</label>
   <div class="col-lg-9">
      <select class="form-control demo-select2" name="subsubcategory_id" id="subsubcategory_id" required>

      </select>
   </div>
</div>
<div class="form-group" id="brand">
   <label class="col-lg-3 control-label">{{__('Brand')}}</label>
   <div class="col-lg-9">
      <select class="form-control demo-select2" name="brand_id" id="brand_id" required>

      </select>
   </div>
</div>
<div class="form-group">
    <label class="col-lg-3 control-label" for="name">{{__('Product')}}</label>
    <div class="col-lg-9">
        <select name="product_ids[]" id="product_id" class="form-control demo-select2" multiple required>

        </select>
    </div>
</div>
<div class="form-group">
    <label class="col-lg-3 control-label" for="start_date">{{__('Date')}}</label>
    <div class="col-lg-9">
        <div id="demo-dp-range">
            <div class="input-daterange input-group" id="datepicker">
                <input type="text" class="form-control" name="start_date" value="{{ date('m/d/Y', $coupon->start_date) }}">
                <span class="input-group-addon">{{__('to')}}</span>
                <input type="text" class="form-control" name="end_date" value="{{ date('m/d/Y', $coupon->end_date) }}">
            </div>
        </div>
    </div>
</div>
<div class="form-group">
   <label class="col-lg-3 control-label">{{__('Discount')}}</label>
   <div class="col-lg-8">
      <input type="number" min="0" step="0.01" value="{{ $coupon_details->discount }}" placeholder="{{__('Discount')}}" name="discount" class="form-control" required>
   </div>
   <div class="col-lg-1">
      <select class="demo-select2" name="discount_type">
         <option value="amount" @if ($coupon_details->discount_type == 'amount') selected  @endif>$</option>
         <option value="percent" @if ($coupon_details->discount_type == 'percent') selected  @endif>%</option>
      </select>
   </div>
</div>


<script type="text/javascript">

    function get_subcategories_by_category(){
		var category_id = $('#category_id').val();
		$.post('{{ route('subcategories.get_subcategories_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
		    $('#subcategory_id').html(null);
		    for (var i = 0; i < data.length; i++) {
		        $('#subcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		        $('.demo-select2').select2();
		    }
            $("#subcategory_id > option").each(function() {
		        if(this.value == '{{$coupon_details->sub_cat_id}}'){
		            $("#subcategory_id").val(this.value).change();
		        }
		    });
		    get_subsubcategories_by_subcategory();
		});
	}

	function get_subsubcategories_by_subcategory(){
		var subcategory_id = $('#subcategory_id').val();
		$.post('{{ route('subsubcategories.get_subsubcategories_by_subcategory') }}',{_token:'{{ csrf_token() }}', subcategory_id:subcategory_id}, function(data){
		    $('#subsubcategory_id').html(null);
		    for (var i = 0; i < data.length; i++) {
		        $('#subsubcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		        $('.demo-select2').select2();
		    }
            $("#subsubcategory_id > option").each(function() {
		        if(this.value == '{{$coupon_details->sub_sub_cat_id}}'){
		            $("#subsubcategory_id").val(this.value).change();
		        }
		    });
            get_brands_by_subsubcategory();
		});
	}

    function get_brands_by_subsubcategory(){
        var subsubcategory_id = $('#subsubcategory_id').val();
        $.post('{{ route('subsubcategories.get_brands_by_subsubcategory') }}',{_token:'{{ csrf_token() }}', subsubcategory_id:subsubcategory_id}, function(data){
            $('#brand_id').html(null);
            for (var i = 0; i < data.length; i++) {
                $('#brand_id').append($('<option>', {
                    value: data[i].id,
                    text: data[i].name
                }));
                $('.demo-select2').select2();
            }
            $("#brand_id > option").each(function() {
		        if(this.value == '{{$coupon_details->brand_id}}'){
		            $("#brand_id").val(this.value).change();
		        }
		    });
            get_products_by_brand();
        });
    }

    function get_products_by_brand(){
        var brand_id = $('#brand_id').val();
        $.post('{{ route('products.get_products_by_brand') }}',{_token:'{{ csrf_token() }}', brand_id:brand_id}, function(data){
            $('#product_id').html(data);
            $("#product_id > option").each(function(em) {
		        if(this.value == '{{$coupon_details->product_id}}'){
		            $("#product_id").val(this.value).change();
		        }
		    });
            $(".demo-select2").select2();
        });
    }

    $(document).ready(function(){
        get_subcategories_by_category();
    });

    $('#category_id').on('change', function() {
        get_subcategories_by_category();
    });

    $('#subcategory_id').on('change', function() {
	    get_subsubcategories_by_subcategory();
	});
    $('#subsubcategory_id').on('change', function() {
        get_brands_by_subsubcategory();
    });
    $('#brand_id').on('change', function() {
        get_products_by_brand();
    });


</script>
