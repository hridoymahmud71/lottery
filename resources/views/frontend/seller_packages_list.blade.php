@extends('frontend.layouts.app')

@section('content')

    <section class="gry-bg py-4 profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    @include('frontend.inc.seller_side_nav')
                </div>

                <div class="col-lg-9">
                    <!-- Page title -->
                    <div class="page-title">
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                    {{__('Packages for sellers')}}
                                </h2>
                            </div>
                            <div class="col-md-6">
                                <div class="float-md-right">
                                    <ul class="breadcrumb">
                                        <li><a href="{{ route('home') }}">{{__('Home')}}</a></li>
                                        <li class="active"><a href="{{ route('dashboard') }}">{{__('Dashboard')}}</a></li>
                                        <li class="active"><a href="{{ route('seller_packages_list_show') }}">{{__('Seller Packages')}}</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- dashboard content -->
                    <div class="">
                        <div class="row ml-auto">
                            <p>If you don't purchase any package you have to pay {{ \App\BusinessSetting::where('type', 'vendor_commission')->first()->value }} percent commission. </p>
                        </div>
                        <div class="row">
                            @foreach ($seller_packages as $key => $seller_package)
                            <div class="col-md-4">
                                <div class="form-box bg-white mt-4">
                                    <div class="form-box-title px-3 py-2 text-center">
                                        <strong>{{$seller_package->name}}</strong>
                                    </div>
                                    <div class="form-box-content p-3 category-widget text-center">
                                        <center><img alt="Package Logo" src="{{ asset($seller_package->logo) }}" style="height:150px; width:130px;"></center>
                                        <strong><span class="d-block title"><h3>{{__('Price')}}: {{ single_price($seller_package->amount) }}</h3></span></strong>
                                        <strong><p>{{__('Basic Product Upload')}}: {{ $seller_package->basic_product }} {{__('Times')}}</p></strong>
                                        <strong><p>{{__('Banner Upload')}}: {{ $seller_package->banner_uploads }} {{__('Times')}}</p></strong>
                                        <strong><p>@if($seller_package->id == $seller->seller_package_id)
                                            <div class="name mb-0">{{__('Current Package')}}: <span class="ml-2"><i class="fa fa-check-circle" style="color:green"></i></span></div>
                                        @else
                                            <div class="name mb-0">{{__('Current Package')}}: <span class="ml-2"><i class="fa fa-times-circle" style="color:red"></i></span></div>
                                        @endif</p></strong>
                                        <div class="text-center">
                                            <button class="btn btn-base-1 w-100" onclick="show_price_modal({{ $seller_package->id}})">{{__('Get This')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="price_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="modal-header">
                    <h5 class="modal-title strong-600 heading-5">{{__('Purchase Your Package')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="" action="{{ route('seller_purchase_package') }}" method="post">
                    @csrf
                    <input type="hidden" name="seller_package_id" value="">
                    <div class="modal-body gry-bg px-3 pt-3">
                        <div class="row">
                            <div class="col-md-2">
                                <label>{{__('Payment Method')}}</label>
                            </div>
                            <div class="col-md-10">
                                <div class="mb-3">
                                    <select class="form-control selectpicker" data-minimum-results-for-search="Infinity" name="payment_option">
                                        <option value="paypal">{{__('Paypal')}}</option>
                                        <option value="stripe">{{__('Stripe')}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('cancel')}}</button>
                        <button type="submit" class="btn btn-base-1">{{__('Confirm')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        function show_price_modal(id, amount){
            $('input[name=seller_package_id]').val(id);
            $('#price_modal').modal('show');
        }
    </script>
@endsection
