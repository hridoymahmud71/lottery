@extends('frontend.layouts.app')

@section('content')

    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('home') }}">{{__('Home')}}</a></li>
                        <li><a href="{{ route('customer.products') }}">{{__('Customer Products')}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <section class="gry-bg py-4">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="products-box-bar p-3 bg-white">
                        <div class="row">
                            @foreach ($customer_products as $key => $customer_product)
                                <div class="col-lg-4 col-md-6">
                                    <div class="product-card-1 mb-3">
                                        <figure class="product-image-container">
                                            <a href="{{ route('customer.product', $customer_product->slug) }}" class="product-image d-block" style="background-image:url('{{ asset($customer_product->thumbnail_img) }}');">
                                            </a>
                                            @if (strtotime($customer_product->created_at) > strtotime('-10 day'))
                                                <span class="product-label label-hot">{{__('New')}}</span>
                                            @endif
                                        </figure>
                                        <div class="product-details text-center">
                                            <h2 class="product-title text-truncate-2">
                                                <a href="{{ route('customer.product', $customer_product->slug) }}">{{ $customer_product->name }}</a>
                                            </h2>
                                            <div class="price-box">
                                                <span class="product-price strong-300"><strong>{{ single_price($customer_product->unit_price) }}</strong></span>
                                            </div><!-- End .price-box -->
                                        </div><!-- End .product-details -->
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="products-pagination bg-white p-3">
                        <nav aria-label="Center aligned pagination">
                            <ul class="pagination justify-content-center">
                                {{ $customer_products->links() }}
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        function filter(){
            $('#search-form').submit();
        }
        function rangefilter(arg){
            $('input[name=min_price]').val(arg[0]);
            $('input[name=max_price]').val(arg[1]);
            filter();
        }
    </script>
@endsection
