@extends('frontend.layouts.app')

@section('content')
    <div class="row m-5">
        @foreach ($customer_packages as $key => $customer_package)
            <div class="col-md-3">
                <div class="dashboard-widget text-center mt-4 c-pointer">
                    <img alt="Package Logo" src="{{ asset($customer_package->logo) }}" style="height:200px;">
                    <span class="d-block title">{{ $customer_package->name }}</span>
                    <strong><p>{{__('Product Upload')}}: {{ $customer_package->product_upload }} {{__('Times')}}</p></strong>
                    <span class="d-block title">{{__('Price')}}: {{ single_price($customer_package->amount) }}</span>
                    <hr>
                    {{-- <a class="btn btn-anim-primary w-100" onclick="show_price_modal({{ $customer_package->id }})">{{__('Get This')}}</a> --}}
                    <button class="btn btn-base-1 w-100" onclick="show_price_modal({{ $customer_package->id}})">{{__('Get This')}}</button>
                </div>
            </div>
        @endforeach
    </div>

    <div class="modal fade" id="price_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="modal-header">
                    <h5 class="modal-title strong-600 heading-5">{{__('Purchase Your Package')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="" action="{{ route('purchase_package') }}" method="post">
                    @csrf
                    <input type="hidden" name="customer_package_id" value="">
                    <div class="modal-body gry-bg px-3 pt-3">
                        <div class="row">
                            <div class="col-md-2">
                                <label>{{__('Payment Method')}}</label>
                            </div>
                            <div class="col-md-10">
                                <div class="mb-3">
                                    <select class="form-control selectpicker" data-minimum-results-for-search="Infinity" name="payment_option">
                                        <option value="paypal">{{__('Paypal')}}</option>
                                        <option value="stripe">{{__('Stripe')}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('cancel')}}</button>
                        <button type="submit" class="btn btn-base-1">{{__('Confirm')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        function show_price_modal(id, amount){
            $('input[name=customer_package_id]').val(id);
            $('#price_modal').modal('show');
        }
    </script>
@endsection
