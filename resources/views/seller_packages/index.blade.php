@extends('layouts.app')

@section('content')

<div class="row">
    @foreach ($seller_packages as $key => $seller_package)
        <div class="col-lg-4">
            <div class="panel">
                <div class="panel-body text-center">
                    <img alt="Package Logo" class="img-lg img-circle mar-btm" src="{{ asset($seller_package->logo) }}">
                    <p class="text-lg text-semibold mar-no text-main">{{$seller_package->name}}</p>
                    <p class="text-3x">{{single_price($seller_package->amount)}}</p>
                    <p class="text-sm text-overflow pad-top">
                         Basic Product Upload:
                        <span class="text-bold">{{$seller_package->basic_product}}</span>
                    </p>
                    <p class="text-sm text-overflow pad-top">
                         Banner Upload:
                        <span class="text-bold">{{$seller_package->banner_uploads}}</span>
                    </p>
                    <div class="mar-top">
                        <a href="{{route('seller_packages.edit', encrypt($seller_package->id))}}" class="btn btn-mint">{{__('Edit')}}</a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>


@endsection
