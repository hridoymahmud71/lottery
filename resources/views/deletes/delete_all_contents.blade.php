@extends('layouts.app')

@section('content')

    <div class="row">
        <h2 style="margin: 10px;">{{ __('Alert!') }}</h2>
        <div class="col-lg-12 text-center">
            @if ($type == 'delete_all_category')
                <h3>{{__('Are You Sure You Want To Delete All Category')}}</h3>
                <a href="{{ route('delete_all_contents.confirm', $type) }}" class="btn btn-rounded btn-danger ">{{__('Delete All Category')}}</a>
            @elseif ($type == 'delete_all_subcategories')
                <h3>{{__('Are You Sure You Want To Delete All Sub-Category')}}</h3>
                <a href="{{ route('delete_all_contents.confirm', $type) }}" class="btn btn-rounded btn-danger">{{__('Delete All Sub-Category')}}</a>
            @elseif ($type == 'delete_all_subsubcategories')
                <h3>{{__('Are You Sure You Want To Delete All Sub-Sub-Category')}}</h3>
                <a href="{{ route('delete_all_contents.confirm', $type) }}" class="btn btn-rounded btn-danger">{{__('Delete All Sub-Sub-Category')}}</a>
            @elseif ($type == 'delete_all_brands')
                <h3>{{__('Are You Sure You Want To Delete All Brands')}}</h3>
                <a href="{{ route('delete_all_contents.confirm', $type) }}" class="btn btn-rounded btn-danger">{{__('Delete All Brands')}}</a>
            @elseif ($type == 'delete_all_classified')
                <h3>{{__('Are You Sure You Want To Delete All Classified')}}</h3>
                <a href="{{ route('delete_all_contents.confirm', $type) }}" class="btn btn-rounded btn-danger">{{__('Delete All Classified')}}</a>
            @endif

        </div>
    </div>

@endsection
